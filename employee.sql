-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2022 at 08:04 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajar`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(40) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `phone` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `email`, `gender`, `phone`) VALUES
(9, ' julo Savier', 'julo_Savier@yahoo.com', 'Man', '6297654342'),
(13, ' Xavix Savier', 'Xavi_Savier@yahoo.com', 'Man', '6297654342'),
(14, ' Xavix Savier', 'Xavi_Savier@yahoo.com', 'Man', '6297654342'),
(15, ' Xavix Savier', 'Xavi_Savier@yahoo.com', 'Man', '6297654342'),
(16, ' Xavix Savier', 'Xavi_Savier@yahoo.com', 'Man', '6297654342');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


<?xml version="1.0"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=169433
  -->
<configuration>
  <configSections/>
  <connectionStrings>

    <!--Local / Dev Database-->
    <!--<add name="IFRS16" connectionString="Data Source=.;Initial catalog=IFRS16;User ID=sa;Password=sa12345;Persist Security Info=false;Connect Timeout=360000;" />
    <add name="COC_DBConnectionString"
      connectionString="Data Source=.;Initial Catalog=IFRS16;Persist Security Info=True;User ID=sa;Password=sa12345"
      providerName="System.Data.SqlClient"/>-->
      <add name="IFRS16" connectionString="Data Source=10.197.18.12\UAT1;Initial catalog=IFRS16;User ID={0};Password={1};Persist Security Info=false;Connect Timeout=360000;" />
    <add name="COC_DBConnectionString"
      connectionString="Data Source=10.197.18.12\UAT1;Initial Catalog=IFRS16;Persist Security Info=True;User ID=sa;Password=sa12345"
      providerName="System.Data.SqlClient"/>
    
    
    
 </connectionStrings>
  <appSettings>
    <!--Encryption-->
    <add key="EncryptionKey" value="ifrs"/>
    <add key="EncryptionActive" value="true"/>
    <add key="DbUsername" value="Jiv8DrjhU+l5805EPZvGr836XFR/iWH8dFsCaq+g4/k="/>
    <add key="DbPassword" value="uVYrN+kJW6M3Eorn4NgVQ+H+WNSUZOsDI/Mr24uo69A="/>
    <!--Base-->
    <add key="DatabaseName" value="IFRS16"/>
    <add key="BaseURL" value="https://10.197.18.11:8011/"/>
    <add key="DomainName" value="danamon.dev"/> 
    <!--Development Mode-->
    <add key="BypassAuth" value="true"/>
    <add key="BypassPassword" value="7njf3WZwCeqfTQJIaqnjGg=="/>
    <add key="IsByPassMenu" value="true"/>
    <add key="IsDebugMode" value="false"/>
    <add key="DebugUserID" value="danamon.dev/13032984"/> 
    <!--LDAP-->
    <add key="UseLDAP" value="false"/>
    <add key="LdapAddress" value="LDAP://ldapuat.corpuat.danamon.co.id:389/dc=corpuat,dc=Danamon,dc=co,dc=id"/>
    <add key="LdapAdminUsername" value=""/>
    <add key="LdapAdminPassword" value=""/>
    <!--Location file-->
    <add key="HeadOfficeCode" value="HO00"/>
    <add key="LocationDownloadFile" value="~/Download/"/> 
    <add key="UploadPath" value="~/FileServer/TransactionUpload"/>
    <add key="TempFolder" value="C:\Temp\IFRS\Temp\"/>
    <!--Ultimus Workflow-->
    <add key="WorkflowName" value="Workflow WOW"/>
    <add key="ActivateUltimus" value="false"/>
    <add key="DevelopUltimus" value="true"/>
    <!--Email Code-->
    <add key="Email_Approve" value="EML001"/>
    <add key="Email_Requester_Approve" value="EML002"/>
    <add key="Email_Requester_Revise" value="EML003"/>
    <add key="Email_Requester_Reject" value="EML004"/>
    <add key="MAINGUID" value="3BF559B47509DE0E1F54CF924B0EF72C7F2FCF44"/>
    <add key="UGUID" value="LkhgP3nd/v/RmuTAARNU2g=="/>
    <add key="PGUID" value="Zsm4Y5YXbvT18cKpv8maIIU8EodRAzZSGdEFBldQmsQ="/>
    <!--Excel Protection-->
    <add key="ExcelProtection" value="hunter2"/>
    <add key="IsAllowMultipleLogin" value="true"/>
    <add key="IsByPassLogin" value="true"/>
    <add key="WebType" value="WebApps"/>
  </appSettings>
  <!--
    For a description of web.config changes for .NET 4.5 see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.5" />
      </system.Web>
  -->
  <system.web>
    <httpCookies httpOnlyCookies="true" requireSSL="true"/>
    <!--<customErrors mode="Off"/>-->
    <!--<httpRuntime maxRequestLength="1048576" />-->
    <httpHandlers>
      <add verb="*" path="Reserved.ReportViewerWebControl.axd"
        type="Microsoft.Reporting.WebForms.HttpHandler, Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"/>
    </httpHandlers>
    <!--Session-->
    <!--<sessionState mode="SQLServer" allowCustomSqlDatabase="true" timeout="20" sqlConnectionString="Data Source=CODEID\SQLSERVER12; Initial Catalog=ASPState; User ID=sa; Password=1ND0nesia;"/>-->
    <!--<sessionState mode="SQLServer" allowCustomSqlDatabase="true" timeout="20" sqlConnectionString="Data Source=192.168.43.39\MSSQLSERVER2012; Initial Catalog=ASPState; User ID=sa; Password=P@ssw0rd;"/>-->
    <!--<sessionState mode="SQLServer" allowCustomSqlDatabase="true" timeout="20" sqlConnectionString="Data Source=DESKTOP-2UJM4DJ\SQLSERVER2012; Initial Catalog=ASPState; User ID=sa; Password=P@ssw0rd;"/>-->
    <!--<sessionState mode="InProc" stateNetworkTimeout="60" timeout="60"/>-->
          <sessionState timeout="20"></sessionState>

    <compilation targetFramework="4.5">
      <assemblies>
        <add assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"/>
        <add assembly="Microsoft.ReportViewer.Common, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"/>
        <add assembly="Microsoft.Build.Framework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"/>
      </assemblies>
    </compilation>
    <pages enableEventValidation="false" controlRenderingCompatibilityVersion="4.0">
      <controls>
        <!--Controls-->
        <add tagPrefix="wow" namespace="IFRS.WebUI.Controls" assembly="IFRS.WebUI"/>
        <add tagPrefix="wow" tagName="WebGridView" src="~/Controls/WebGridView.ascx"/>
        <add tagPrefix="wow" tagName="Breadcrumb" src="~/Controls/Breadcrumb.ascx"/>
        <add tagPrefix="wow" tagName="FileAttachmentList" src="~/Controls/FileAttachmentList.ascx"/>
        <add tagPrefix="iwc" assembly="Integrasi.WebControl" namespace="Integrasi.WebControl"/>
        <!--End Controls-->
        <!--Modal-->
        <add tagPrefix="wow" tagName="ModalUser" src="~/Modals/ModalUser.ascx"/>
        <add tagPrefix="wow" tagName="ModalJobs" src="~/Modals/ModalJobs.ascx"/>
        <add tagPrefix="wow" tagName="ModalWorkflowConditions" src="~/Modals/ModalWorkflowConditions.ascx"/>
        <add tagPrefix="modal" tagName="ModalVendor" src="~/Modals/ModalVendor.ascx"/>
        <add tagPrefix="modal" tagName="ModalZIP" src="~/Modals/ModalZIP.ascx"/>
        <add tagPrefix="modal" tagName="ModalVendorServices" src="~/Modals/ModalVendorServices.ascx"/>
        <add tagPrefix="modal" tagName="ModalCardStock" src="~/Modals/ModalCardStock.ascx"/>
        <add tagPrefix="modal" tagName="ModalKecamatan" src="~/Modals/ModalKecamatan.ascx"/>
        <!--End Modal-->
      </controls>
    </pages>
    <authentication mode="Forms">
      <forms loginUrl="~/Default.aspx" defaultUrl="~/Pages/Home.aspx" name=".Monitoring_AUTH"/>
    </authentication>
    <!--<authorization>
      <deny users="?"/>
    </authorization>-->
    <!--validion mode-->
    <httpRuntime maxRequestLength="1048576" executionTimeout="3600"/>
    <webServices>
      <protocols>
        <add name="HttpPost"/>
        <add name="HttpGet"/>
      </protocols>
    </webServices>
    <identity impersonate="true"/>
    <globalization culture="en-US" uiCulture="en-US"/>
  </system.web>
  <location path="Assets">
    <system.web>
      <authorization>
        <allow users="*"/>
      </authorization>
    </system.web>
  </location>
  <system.webServer>
    <httpProtocol>
      <customHeaders>
        <add name="X-Content-Type-Options" value="nosniff"/>
        <add name="Strict-Transport-Security" value="max-age=31536000"/>
        <add name="X-Frame-Options" value="SAMEORIGIN"/>
      </customHeaders>
    </httpProtocol>
    <!--<modules runAllManagedModulesForAllRequests="true" />-->
    <handlers>
      <!--<add name="ReportViewerWebControlHandler" preCondition="integratedMode" verb="*" path="Reserved.ReportViewerWebControl.axd" type="Microsoft.Reporting.WebForms.HttpHandler, Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" />-->
      <add name="ReportViewerWebControlHandler" preCondition="integratedMode" verb="*" path="Reserved.ReportViewerWebControl.axd"
        type="Microsoft.Reporting.WebForms.HttpHandler, Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"/>
    </handlers>
    <validation validateIntegratedModeConfiguration="false"/>
    <staticContent>
      <remove fileExtension=".woff"/>
      <remove fileExtension=".woff2"/>
      <!-- In case IIS already has this mime type -->
      <mimeMap fileExtension=".woff" mimeType="application/x-font-woff"/>
      <mimeMap fileExtension=".woff2" mimeType="application/x-font-woff2"/>
    </staticContent>
  </system.webServer>
  <!--<runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="NPOI" publicKeyToken="0df73ec7942b34e1" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-2.2.1.0" newVersion="2.2.1.0"/>
      </dependentAssembly>
    </assemblyBinding>
  </runtime>-->
  <system.codedom>
    <compilers>
      <compiler language="c#;cs;csharp" extension=".cs"
        type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
        warningLevel="4" compilerOptions="/langversion:6 /nowarn:1659;1699;1701"/>
      <compiler language="vb;vbs;visualbasic;vbscript" extension=".vb"
        type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.VBCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
        warningLevel="4" compilerOptions="/langversion:14 /nowarn:41008 /define:_MYTYPE=\&quot;Web\&quot; /optionInfer+"/>
    </compilers>
  </system.codedom>
</configuration>
